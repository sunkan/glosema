<?php

namespace Glosema\MongoDb;

use Glosema\Query;
use MongoDB\Client as BaseClient;

class Client extends BaseClient
{
    /**
     * Return Query object
     *
     * @return Glosema\Query
     */
    public function getQuery()
    {
        return new Query();
    }
}
