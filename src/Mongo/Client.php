<?php

namespace Glosema\Mongo;

use Glosema\Query;
use MongoClient;

class Client extends MongoClient
{
    /**
     * Return Query object
     *
     * @return Glosema\Query
     */
    public function getQuery()
    {
        return new Query();
    }
}
